﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour {
	
	private ShowPanels showPanels;						//Reference to the ShowPanels script used to hide and show UI panels
	private bool isPaused;								//Boolean to check if the game is paused or not
	private StartOptions startScript;					//Reference to the StartButton script
	private QuitApplication quitScript;					// Reference to the QuitApplication script;
	private PlayMusic playMusic;						//Reference to PlayMusic script
	private BoardView boardView;
	private bool clickHandler;

	//Awake is called before Start()
	void Awake()
	{
		//Get a component reference to ShowPanels attached to this object, store in showPanels variable
		showPanels = GetComponent<ShowPanels> ();
		//Get a component reference to StartButton attached to this object, store in startScript variable
		startScript = GetComponent<StartOptions> ();
		//Get a component reference to QuitApplication attached to this object, store in quitScript variable
		quitScript = GetComponent<QuitApplication> ();
		//Get a reference to PlayMusic attached to UI object
		playMusic = GetComponent<PlayMusic> ();
		// get the boardview from the main camera
		boardView = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<BoardView>();
	}

	// Update is called once per frame
	void Update () {

		//Check if the Cancel button in Input Manager is down this frame (default is Escape key) and that game is not paused, and that we're not in main menu
		if (Input.GetButtonDown ("Cancel") && !isPaused && !startScript.inMainMenu) 
		{
			//Call the DoPause function to pause the game
			DoPause();
		} 
		//If the button is pressed and the game is paused and not in main menu
		else if (Input.GetButtonDown ("Cancel") && isPaused && !startScript.inMainMenu) 
		{
			//Call the UnPause function to unpause the game
			UnPause ();
		}
	
	}


	public void DoPause()
	{
		// get the boardview from the main camera
		boardView = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<BoardView>();
		// remember value to restore it after pause
		clickHandler = boardView.clickHandlersEnabled;
		// disable clicks on boardview
		boardView.clickHandlersEnabled = false;
		//Set isPaused to true
		isPaused = true;
		//Set time.timescale to 0, this will cause animations and physics to stop updating
		Time.timeScale = 0;
		//call the ShowPausePanel function of the ShowPanels script
		showPanels.ShowPausePanel ();
	}


	public void UnPause()
	{
		//Set isPaused to false
		isPaused = false;
		//Set time.timescale to 1, this will cause animations and physics to continue updating at regular speed
		Time.timeScale = 1;
		// restore remembered value
		boardView.clickHandlersEnabled = clickHandler;
		//call the HidePausePanel function of the ShowPanels script
		showPanels.HidePausePanel ();
	}

	public void MainMenu() {
		UnPause();
		FadeOut();
		//Use invoke to delay calling of Destroy by 100% the length of fadeColorAnimationClip
		Invoke("LoadMainMenu", startScript.fadeColorAnimationClip.length * 1.0f);
	}

	private void LoadMainMenu() {
		Destroy(this.gameObject);
		SceneManager.LoadScene(0);
	}

	public void FadeQuit() {
		UnPause();
		FadeOut();
		//Use invoke to delay calling of LoadDelayed by half the length of fadeColorAnimationClip
		Invoke ("Quit", startScript.fadeColorAnimationClip.length * 0.5f);
	}
	
	private void FadeOut() {
		//let the music fade out
		playMusic.FadeDown(startScript.fadeColorAnimationClip.length);
		//Set the trigger of Animator animColorFade to start transition to the FadeToOpaque state.
		startScript.animColorFade.SetTrigger("fade");
	}

	private void Quit() {
		quitScript.Quit();
	}
}
