﻿using UnityEngine;
using System.Collections;

public class ShowPanels : MonoBehaviour {

	public GameObject optionsPanel;							//Store a reference to the Game Object OptionsPanel 
	public GameObject optionsTint;							//Store a reference to the Game Object OptionsTint 
	public GameObject menuPanel;							//Store a reference to the Game Object MenuPanel 
	public GameObject pausePanel;							//Store a reference to the Game Object PausePanel 
	public GameObject newGamePanel;							//Store a reference to the Game Object NewGamePanel
	public GameObject botGamePanel;							//Store a reference to the Game Object BotGamePanel
	public GameObject customGamePanel;						//Store a reference to the Game Object CustomGamePanel


	//Call this function to activate and display the Options panel during the main menu
	public void ShowOptionsPanel()
	{
		optionsPanel.SetActive(true);
		optionsTint.SetActive(true);
	}

	//Call this function to deactivate and hide the Options panel during the main menu
	public void HideOptionsPanel()
	{
		optionsPanel.SetActive(false);
		optionsTint.SetActive(false);
	}

	//Call this function to activate and display the main menu panel during the main menu
	public void ShowMenu()
	{
		menuPanel.SetActive (true);
	}

	//Call this function to deactivate and hide the main menu panel during the main menu
	public void HideMenu()
	{
		menuPanel.SetActive (false);
	}
	
	//Call this function to activate and display the Pause panel during game play
	public void ShowPausePanel()
	{
		pausePanel.SetActive (true);
		optionsTint.SetActive(true);
	}

	//Call this function to deactivate and hide the Pause panel during game play
	public void HidePausePanel()
	{
		pausePanel.SetActive (false);
		optionsTint.SetActive(false);

	}

	// Call this function to activate and display the NewGame panel during the main menu
	public void ShowNewGamePanel() {
		newGamePanel.SetActive(true);
		optionsTint.SetActive(true);
	}

	// Call this function to deactivate and hide the NewGame panel during the main menu
	public void HideNewGamePanel() {
		newGamePanel.SetActive(false);
		optionsTint.SetActive(false);
	}

	// Call this function to activate and display the BotGamePanel panel during the main menu
	public void ShowBotGamePanel() {
		botGamePanel.SetActive(true);
	}

	// Call this function to deactivate and hide the BotGamePanel panel during the main menu
	public void HideBotGamePanel() {
		botGamePanel.SetActive(false);
	}

	// Call this function to activate and display the CustomGamePanel panel during the main menu
	public void ShowCustomGamePanel() {
		customGamePanel.SetActive(true);
	}

	// Call this function to deactivate and hide the CustomGamePanel panel during the main menu
	public void HideCustomGamePanel() {
		customGamePanel.SetActive(false);
	}
}
