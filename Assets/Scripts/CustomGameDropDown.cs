﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CustomGameDropDown : MonoBehaviour {

	public GameObject playerDropdown;
	public GameObject evaluationFunctionDropdown;
	public GameObject depthSlider;

	void Awake () {
		Update();
	}

	// Update is called once per frame
	void Update () {
		if (playerDropdown.GetComponent<Dropdown>().value == 0) {
			evaluationFunctionDropdown.SetActive(false);
			depthSlider.SetActive(false);
		} else {
			evaluationFunctionDropdown.SetActive(true);
			depthSlider.SetActive(true);
		}
	}
}
