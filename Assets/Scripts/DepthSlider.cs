﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DepthSlider : MonoBehaviour {

	public GameObject Text;
	public GameObject Slider;

	void Awake () {
		Update();
	}

	// Update is called once per frame
	void Update () {
		Text.GetComponent<Text>().text = "depth: " + Slider.GetComponent<Slider>().value;
	}
}
