
public struct StartPoint {

	private Point point;
	private Player color;

	public int X {
		get {
			return this.point.X;
		}
	}

	public int Y {
		get {
			return this.point.Y;
		}
	}

	public Player Color {
		get {
			return this.color;
		}
	}

	public StartPoint(int x, int y, Player color) {
		this.point = new Point(x, y);
		this.color = color;
	}

	public StartPoint(Point point, Player color) {
		this.point = point;
		this.color = color;
	}
}
