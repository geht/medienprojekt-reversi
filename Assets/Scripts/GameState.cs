using System;
using System.Collections.Generic;

public class GameState {

	private static readonly Point[] directions = {	new Point(-1, -1), new Point(-1, 0), new Point(-1, 1),
													new Point(0, -1), new Point(0, 1),
													new Point(1, -1), new Point(1, 0), new Point(1, 1)};

	private Player currentPlayer;
	private Player opponent;
	private Player[,] board;
	private int[] score;
	private List<Point> validMoves;

	public Player CurrentPlayer {
		get {
			return this.currentPlayer;
		}
	}

	public Player Opponent {
		get {
			return this.opponent;
		}
	}

	public Player[,] Board {
		get {
			return this.board;
		}
	}

	public int[] Score {
		get {
			return this.score;
		}
	}

	public List<Point> ValidMoves {
		get {
			return this.validMoves;
		}
	}

	public GameState() {
		Initialize();
		Setup();
	}

	// copy constructor
	public GameState(GameState gs) {
		Initialize();

		currentPlayer = gs.currentPlayer;
		opponent = gs.opponent;
		Array.Copy(gs.board, board, gs.board.Length);
		Array.Copy(gs.score, score, gs.score.Length);
		validMoves = new List<Point>(gs.ValidMoves);
	}

	private void Initialize() {
		currentPlayer = new Player();
		opponent = new Player();
		board = new Player[Game.BOARDSIZE, Game.BOARDSIZE];
		score = new int[2];
		validMoves = new List<Point>();
	}

	private void Setup() {
		// black always begins
		currentPlayer = Player.BLACK;
		opponent = Player.WHITE;

		// setup the board
		for (int y = 0; y < Game.BOARDSIZE; y++) {
			for (int x = 0; x < Game.BOARDSIZE; x++) {
				board[x, y] = Player.NONE;
			}
		}

		// setup start position
		foreach (StartPoint startPoint in Game.STARTPOINTS) {
			board[startPoint.X, startPoint.Y] = startPoint.Color;
		}

		// setup score
		UpdateScore();

		// setup valid moves
		ComputeValidMoves();
	}

	private void UpdateScore() {
		score[(int) Player.BLACK] = 0;
		score[(int) Player.WHITE] = 0;
		foreach (Player player in board) {
			if (player != Player.NONE) {
				score[(int) player]++;
			}
		}
	}

	private void ComputeValidMoves() {
		// clear old list first
		validMoves.Clear();

		// iterate over board
		for (int y = 0; y < Game.BOARDSIZE; y++) {
			for (int x = 0; x < Game.BOARDSIZE; x++) {

				// skip if field doesn't belong to the enemy
				if (board[x, y] != opponent) {
					continue;
				}

				// iterate over adjacent fields
				for (int t = y - 1; t < y + 2; t++) {
					for (int s = x - 1; s < x + 2; s++) {

						// skip if out of bounds or field isn't empty
						if (!isInBounds(s, t) ||  board[s, t] != Player.NONE) {
							continue;
						}

						// check move validity in every direction
						// ((-1,-1), (-1, 0), (-1, 1), ...)
						foreach (Point dir in directions) {						
							Point pos = new Point(s + dir.X, t + dir.Y);
							bool foundTurnableDiscs = false;

							// search for takable discs and move along direction
							while (isInBounds(pos.X, pos.Y) && board[pos.X, pos.Y] == opponent) {
								foundTurnableDiscs = true;
								pos.X += dir.X;
								pos.Y += dir.Y;
							}

							// check if there is own disc on the last position
							if (foundTurnableDiscs && isInBounds(pos.X, pos.Y) && board[pos.X, pos.Y] == currentPlayer) {
								Point move = new Point(s, t);

								// add move only if it isn't already in the list
								if (!validMoves.Contains(move)) {
									validMoves.Add(new Point(s, t));
								}
							}
						}
					}
				}
			}
		}
	}

	public void ExecuteMove(Point move) {
		if (!validMoves.Contains(move) || currentPlayer == Player.NONE) {
			return;
		}

		board[move.X, move.Y] = currentPlayer;

		// check in every direction for turnable discs
		// ((-1,-1), (-1, 0), (-1, 1), ...)
		foreach (Point dir in directions) {
			List<Point> turnableDiscs = new List<Point>();						
			Point pos = new Point(move.X + dir.X, move.Y + dir.Y);

			// search for takable discs and move along direction
			while (isInBounds(pos.X, pos.Y) && board[pos.X, pos.Y] == opponent) {
				turnableDiscs.Add(new Point(pos.X, pos.Y));
				pos.X += dir.X;
				pos.Y += dir.Y;
			}

			// check if there is own disc on the last position
			if (turnableDiscs.Count != 0 && isInBounds(pos.X, pos.Y) && board[pos.X, pos.Y] == currentPlayer) {
				// turn the discs
				foreach (Point p in turnableDiscs) {
					board[p.X, p.Y] = currentPlayer;
				}
			}
		}

		// update the score
		UpdateScore();

		// move on to the next turn
		// so change to the next player and determine which moves are possible
		opponent = currentPlayer;
		currentPlayer = Game.GetOpponent(currentPlayer);
		ComputeValidMoves();

		// if you can't do anything then it's the next players turn
		if (validMoves.Count == 0) {
			opponent = currentPlayer;
			currentPlayer = Game.GetOpponent(currentPlayer);
			ComputeValidMoves();

			// if he can't do anything either the game is over
			// thus there is no next player
			if (validMoves.Count == 0) {
				currentPlayer = Player.NONE;
			}
		}
	}

	private bool isInBounds(int x, int y) {
		return (x >= 0 && x < Game.BOARDSIZE && y >= 0 && y < Game.BOARDSIZE);
	}
}
