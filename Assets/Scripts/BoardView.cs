﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BoardView : MonoBehaviour {

	public Game game;
	public GameObject discPrefab;
	public GameObject discParent;
	public GameObject holoDiscParent;
	public AnimationClip flipAnimationClip;
	public float flipAnimationClipSpeed;
	public AnimationClip spawnAnimationClip;
	public float spawnAnimationClipSpeed;

	public string[] PLAYER_TEXT = {"black", "white"};
	public string[] TURN_TEXT = {"turn: ", "turn: "};
	public Color[] TURN_COLOR = {Color.black, Color.white};
	public string[] SCORE_TEXT = {" score: ", " score: "};
	public string PLACE_TEXT = " disc placed @ ";
	public string INVALID_MOVE_TEXT = "you can not place a disc @ ";
	public string GAMEOVER_TEXT = "game over";
	public string WIN_TEXT = " won!";
	public string TIE_TEXT = "It's a tie!";
	public float SCROLLVIEWCONTENTHEIGHT = 150.0f;
	public float SCROLLMULTIPLIER = 16.0f;
	public int SCROLLTHRESHOLD = 7;

	public bool showTurnLabel;
	public bool showScoreBoard;
	public bool showLog;
	public bool showValidMoves;
	public bool showGears;
	public bool clickHandlersEnabled;

	public bool animationsFinished {
		get {
			if (discParent == null) {
				return false;
			}

			int childCount = discParent.transform.childCount;
			for (int i = 0; i < discParent.transform.childCount; i++) {
				if (!discParent.transform.GetChild(i).GetComponent<DiscAnimationInfo>().animationFinished) {
					return false;
				}
			}

			if (childCount != discParent.transform.childCount) {
				return false;
			}

			return true;
		}
	}

	private int loggedlines;
	private GameObject[,] board;
	private GameObject gears;
	private GameObject ScrollView;
	private GameObject turnText;
	private GameObject[] score;
	private GameObject history;
	private Transform scrollViewContent;
	private int spawnCounter = 0;

	void Start () {
		this.board = new GameObject[Game.BOARDSIZE, Game.BOARDSIZE];

		// put the fields in the array
		GameObject fields = GameObject.Find("Fields");
		for (int y = 0; y < fields.transform.childCount; y++) {
			for (int x = 0; x < fields.transform.GetChild(y).childCount; x++) {
				board[x, y] = fields.transform.GetChild(y).GetChild(x).gameObject;
			}
		}

		// attach ClickHandler scripts to the fields
		foreach (GameObject field in board) {
			ClickHandler handler = field.GetComponent<ClickHandler>();

			if (handler == null) {
				field.AddComponent<ClickHandler>();
			}

			handler = field.GetComponent<ClickHandler>();
			handler.SetView(this);
		}

		turnText = GameObject.Find("UI").transform.Find("Turn Text").gameObject;
		score = new GameObject[2];
		score[0] = GameObject.Find("UI").transform.Find("Score Black").gameObject;
		score[1] = GameObject.Find("UI").transform.Find("Score White").gameObject;
		ScrollView = GameObject.Find("UI").transform.Find("Scroll View").gameObject;
		gears = GameObject.Find("UI").transform.Find("Gears").gameObject;
		scrollViewContent = GameObject.Find("UI").transform.Find("Scroll View/Viewport/Content");
		history = GameObject.Find("UI").transform.Find("Scroll View/Viewport/Content/History Text").gameObject;
	}

	void Update () {
		spawnCounter = 0;

		// update the click handlers
		UpdateClickHandlers();

		// update the board
		UpdateBoardView();

		// update the score
		UpdateScoreView();

		// update the turn label
		UpdateTurnLabel();

		// update valid moves preview
		UpdateValidMovesView();

		// update log
		UpdateLogView();

		// update gear view
		UpdateGearView();
	}

	private void UpdateBoardView() {
		Player[,] boardData = game.CurrentGameState.Board;

		int flipCounter = 0;
		for (int y = 0; y < board.GetLength(1); y++) {
			for (int x = 0; x < board.GetLength(0); x++) {

				if (boardData[x, y] == Player.NONE) {
					continue;
				}

				GameObject disc = board[x, y].GetComponent<Field>().disc;
				// if there is no disc then spawn one
				if (disc == null) {
					AppendLog(PLAYER_TEXT[(int) boardData[x, y]] + PLACE_TEXT + board[x, y].transform.name + board[x, y].transform.parent.name);
					Spawn(board[x, y].transform, boardData[x, y]);
					continue;
				}

				// otherwise turn it if neccessary
				Player color = board[x, y].GetComponent<Field>().color;
				if (disc != null && color != boardData[x, y]) {
					// GameObject.Destroy(disc);
					// Spawn(board[x, y].transform, boardData[x, y]);

					board[x, y].GetComponent<Field>().disc = disc;
					// set the field to the correct color
					board[x, y].GetComponent<Field>().color = boardData[x, y];


					// animate the flipping
					disc.GetComponent<DiscAnimationInfo>().startAnimation();
					StartCoroutine(DoFlip(disc, flipCounter * flipAnimationClip.length * (1.0f / flipAnimationClipSpeed)));
					flipCounter++;
				}
			}
		}
	}

	private IEnumerator DoFlip(GameObject disc, float delay) {
		yield return new WaitForSeconds(delay);
		disc.GetComponent<Animator>().SetTrigger("doFlip");
	}

	private void Spawn(Transform fieldTransform, Player color) {
		// instanciate the disc
		GameObject disc = (GameObject) GameObject.Instantiate(discPrefab);

		// place it at correct position with correct rotation
		disc.transform.GetChild(0).position += fieldTransform.position;

		fieldTransform.GetComponent<Field>().disc = disc;
		fieldTransform.GetComponent<Field>().color = color;

		if (discParent != null)
			disc.transform.parent = discParent.transform;
		else
			Debug.LogWarning("a disc was spawned but couldn't be added to the parent object");

		disc.GetComponent<DiscAnimationInfo>().startAnimation();
		disc.transform.GetChild(0).gameObject.SetActive(false);
		if (color == Player.BLACK) {
			StartCoroutine(DoSpawn(disc, "placeBlack", spawnCounter * spawnAnimationClip.length * (1.0f / spawnAnimationClipSpeed)));
		} else {
			StartCoroutine(DoSpawn(disc, "placeWhite", spawnCounter * spawnAnimationClip.length * (1.0f / spawnAnimationClipSpeed)));
		}
		spawnCounter++;
	}

	private IEnumerator DoSpawn(GameObject disc, string trigger, float delay) {
		yield return new WaitForSeconds(delay);
		disc.transform.GetChild(0).gameObject.SetActive(true);
		disc.GetComponent<Animator>().SetTrigger(trigger);
	}

	private void UpdateValidMovesView() {
		holoDiscParent.SetActive(showValidMoves);

		foreach (Transform t in holoDiscParent.transform) {
			GameObject.Destroy(t.gameObject);
		}

		if (!showValidMoves || !animationsFinished) return;

		foreach (Point move in game.CurrentGameState.ValidMoves) {
			GameObject holoDisc;
			Quaternion rotation = Quaternion.identity;

			if (game.CurrentGameState.CurrentPlayer == Player.BLACK)
				rotation = Quaternion.AngleAxis(180.0f, Vector3.right);

			holoDisc = (GameObject) GameObject.Instantiate(discPrefab);
			holoDisc.transform.GetChild(0).position += board[move.X, move.Y].transform.position;
			holoDisc.transform.GetChild(0).rotation = Quaternion.identity * rotation;
			holoDisc.layer = 2; // hides disc from raycast and hence from clicks
			foreach (MeshRenderer renderer in holoDisc.GetComponentsInChildren<MeshRenderer>()) {
				Color color = renderer.material.color;
				color.a = 0.15f; // set alpha
				renderer.material.color = color;
				renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			}

			if (holoDiscParent != null)
				holoDisc.transform.parent = holoDiscParent.transform;
			else
				Debug.LogWarning("a disc was spawned but couldn't be added to the parent object");
		}
	}

	private void UpdateScoreView() {
		if (score == null || score[(int) Player.BLACK] == null || score[(int) Player.WHITE] == null) return;

		score[(int) Player.BLACK].SetActive(showScoreBoard);
		score[(int) Player.WHITE].SetActive(showScoreBoard);

		if (!showScoreBoard) return;

		score[(int) Player.BLACK].GetComponent<Text>().text = PLAYER_TEXT[(int) Player.BLACK] + SCORE_TEXT[(int) Player.BLACK] + game.CurrentGameState.Score[(int) Player.BLACK];
		score[(int) Player.WHITE].GetComponent<Text>().text = PLAYER_TEXT[(int) Player.WHITE] + SCORE_TEXT[(int) Player.WHITE] + game.CurrentGameState.Score[(int) Player.WHITE];
	}

	private void UpdateTurnLabel() {

		turnText.SetActive(showTurnLabel);
		if (!showTurnLabel) return;

		Player current = game.CurrentGameState.CurrentPlayer;
		Text t = turnText.GetComponent<Text>();

		if (current == Player.NONE) {
			t.color = Color.black;
			t.fontStyle = FontStyle.Bold;
			t.text = GAMEOVER_TEXT;
		} else {
			t.color = TURN_COLOR[(int) current];
			t.text = TURN_TEXT[(int) current] + PLAYER_TEXT[(int) current];
		}
	}

	private void UpdateLogView() {
		ScrollView.gameObject.SetActive(showLog);
	}

	private void UpdateClickHandlers() {
		if (clickHandlersEnabled) {
			foreach (GameObject field in board) {
				field.layer = 0;
			}
		} else {
			foreach (GameObject field in board) {
				field.layer = 2; // hides field from raycast and hence from clicks
			}
		}
	}

	private void UpdateGearView() {
		gears.SetActive(showGears);
	}

	public void OnFieldClick(GameObject field) {
		for (int y = 0; y < board.GetLength(1); y++) {
			for (int x = 0; x < board.GetLength(0); x++) {
				if (board[x, y].Equals(field)) {
					Point plannedMove = new Point(x, y);
					game.OnClick(plannedMove);
					return;
				}
			}
		}
	}

	public void ClearLog() {
		loggedlines = 0;
		history.GetComponent<Text>().text = "";
		((RectTransform) scrollViewContent).sizeDelta = SCROLLVIEWCONTENTHEIGHT * Vector2.up;
	}

	public void AppendLog(string line) {
		history.GetComponent<Text>().text += line + "\n";
		loggedlines++;

		if (loggedlines > SCROLLTHRESHOLD) {
			((RectTransform) scrollViewContent).sizeDelta += SCROLLMULTIPLIER * Vector2.up;
			scrollViewContent.position += SCROLLMULTIPLIER * Vector3.up;
		}
	}

	public void ShowInvalidMove(Point move) {
		AppendLog(INVALID_MOVE_TEXT + board[move.X, move.Y].transform.name + board[move.X, move.Y].transform.parent.name);
	}

	public void ShowGameOver() {
		StartCoroutine(printGameOver());
	}

	private IEnumerator printGameOver() {
		yield return new WaitForSeconds(0.2f);
		AppendLog(GAMEOVER_TEXT);
		AppendLog(PLAYER_TEXT[(int) Player.BLACK] + SCORE_TEXT[(int) Player.BLACK] + game.CurrentGameState.Score[(int) Player.BLACK]);
		AppendLog(PLAYER_TEXT[(int) Player.WHITE] + SCORE_TEXT[(int) Player.WHITE] + game.CurrentGameState.Score[(int) Player.WHITE]);
		if (game.CurrentGameState.Score[0] > game.CurrentGameState.Score[1]) {
			AppendLog(PLAYER_TEXT[0] + WIN_TEXT);
		} else if (game.CurrentGameState.Score[0] < game.CurrentGameState.Score[1]) {
			AppendLog(PLAYER_TEXT[1] + WIN_TEXT);
		} else {
			AppendLog(TIE_TEXT);
		}
	}
}
