﻿using UnityEngine;
using System.Collections;

public class HintToggle : MonoBehaviour {
	public void onHintToggle(bool value) {
		GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Game>().disableHints = !value;
		GameObject.FindGameObjectWithTag("MainCamera").GetComponent<BoardView>().showValidMoves = value;
	}
}
