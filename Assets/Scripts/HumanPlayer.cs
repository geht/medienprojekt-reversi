
public class HumanPlayer : IPlayer {

	private bool finishedThinking;
	private Point move;

	void Start() {
		this.finishedThinking = false;
	}

	public override void StartThinking () {
		finishedThinking = false;
	}

	public override Point GetMove() {
		return move;
	}

	public void SetMove(Point move) {
		this.move = move;
		this.finishedThinking = true;
	}

	public override bool FinishedThinking() {
		return finishedThinking;
	}
}
