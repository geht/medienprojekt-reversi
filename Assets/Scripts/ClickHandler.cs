using UnityEngine;
using System.Collections;

public class ClickHandler : MonoBehaviour {

	private BoardView view;

	void OnMouseDown() {
		if (GetComponent<Field>().disc == null) {
			view.OnFieldClick(this.gameObject);
		}
	}

	public void SetView(BoardView view) {
		this.view = view;
	}
}
