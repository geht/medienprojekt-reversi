using UnityEngine;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

public class AIPlayer : IPlayer {
	
	// maximum depth for MiniMax
	public int maxdepth;

	// 0 is EvaluateStaticSimple
	// 1 is EvaluateStaticDiskSquare
	public int evaluationFunction;

	public delegate int Evaluate(GameState gs);

	private Game game;
	private BoardView boardView;
	private bool finishedThinking;
	private Point move;
	private Evaluate evaluate;

	// this matrix could be computed instead, because there are a lot of symmetries
	// but it doesn't take up much RAM anyways...
	// + this makes the evaluation function really simple! :D
	private static int[,] diskSquareScoreBoard = {	{  50, -20,  10,   5,   5,  10, -20,  50},
													{ -20, -30,   1,   1,   1,   1, -30, -20},
													{  10,   1,   1,   1,   1,   1,   1,  10},
													{   5,   1,   1,   1,   1,   1,   1,   5},
													{   5,   1,   1,   1,   1,   1,   1,   5},
													{  10,   1,   1,   1,   1,   1,   1,  10},
													{ -20, -30,   1,   1,   1,   1, -30, -20},
													{  50, -20,  10,   5,   5,  10, -20,  50}};

	private Thread workerThread;
	private bool abort;

	void Start () {
		Update();
	}

	void Update() {
		game = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Game>();
		boardView = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<BoardView>();

		switch (evaluationFunction) {
		case 0:
			evaluate = EvaluateStaticSimple;
			break;
		case 1:
			evaluate = EvaluateStaticDiskSquare;
			break;
		default:
			evaluate = EvaluateStaticSimple;
			break;
		}
	}

	void OnDestroy() {
		if (workerThread != null && workerThread.IsAlive) {
			abort = true;
			// workerThread.Abort();
		}
	}

	public override void StartThinking () {
		finishedThinking = false;
		StartCoroutine("ComputeMoveAfterAnimationsFinished");
	}

	IEnumerator ComputeMoveAfterAnimationsFinished() {
		// wait until all animations finished
		while(!boardView.animationsFinished){
			yield return null;
		}

		abort = false;
		workerThread = new Thread(this.ComputeMove);
		workerThread.Start();
	}

	private void ComputeMove() {
		Max(game.CurrentGameState, maxdepth, int.MinValue, int.MaxValue);
		finishedThinking = true;
	}

	int Max(GameState gs, int depth, int alpha, int beta) {
		if (abort) return 0;

		// if at leaf return score
		if (depth == 0 || gs.ValidMoves.Count == 0) {
			return evaluate(gs);
		}

		int max = alpha;
		for (int i = 0; i < gs.ValidMoves.Count; i++) {
			// get a clone of gamestate to simulate moves
			GameState simGS = new GameState(gs);
			simGS.ExecuteMove(gs.ValidMoves[i]);

			// go deeper
			int score = Min(simGS, depth - 1, max, beta);

			// update score if neccessary
			if (score > max) {
				max = score;

				// fail hard beta-cutoff
				// for fail soft return score
				if (max >= beta) break;

				// update move if in top level
				if (depth == maxdepth) {
					move = gs.ValidMoves[i];
				}
			}
		}

		return max;	
	}

	int Min(GameState gs, int depth, int alpha, int beta) {
		if (abort) return 0;

		// if at leaf return score
		if (depth == 0 || gs.ValidMoves.Count == 0) {
			return evaluate(gs);
		}

		int min = beta;
		for (int i = 0; i < gs.ValidMoves.Count; i++) {
			// get a clone of  gamestate to simulate moves
			GameState simGS = new GameState(gs);
			simGS.ExecuteMove(gs.ValidMoves[i]);

			// go deeper
			int score = Max(simGS, depth - 1, alpha, min);

			// update score if neccessary
			if (score < min) {
				min = score;

				// fail hard alpha-cutoff
				// for fail soft return score
				if (min <= alpha) break;
			}
		}

		return min;
	}

	private int EvaluateStaticSimple(GameState gs) {
		Player gsOpponent = gs.Opponent;
		Player gsCurrentPlayer = Game.GetOpponent(gsOpponent);

		int score = gs.Score[(int) gsCurrentPlayer] - gs.Score[(int) gsOpponent];

		if (gsCurrentPlayer != game.CurrentGameState.CurrentPlayer) {
			score *= -1;
		}

		return score;
	}

	private int EvaluateStaticDiskSquare(GameState gs) {
		Player gsOpponent = gs.Opponent;
		Player gsCurrentPlayer = Game.GetOpponent(gsOpponent);

		int sum = 0;
		for (int y = 0; y < Game.BOARDSIZE; y++) {
			for (int x = 0; x < Game.BOARDSIZE; x++) {
				if (gs.Board[x, y] == gsCurrentPlayer) {
					sum+=diskSquareScoreBoard[x, y];
				}
			}
		}

		if (gsCurrentPlayer != game.CurrentGameState.CurrentPlayer) {
			sum *= -1;
		}

		return sum;
	}

	public override bool FinishedThinking() {
		return finishedThinking;
	}

	public override Point GetMove() {
		return move;
	}
}
