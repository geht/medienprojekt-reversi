﻿using UnityEngine;
using System.Collections;

public class PlaySound : MonoBehaviour {

	public AudioSource audioSource;

	void playSound() {
		if (!audioSource.isPlaying) {
			audioSource.Play();
		}
	}

}
