﻿using UnityEngine;
using System.Collections;

public class LogToggle : MonoBehaviour {
	public void onLogToggle(bool value) {
		GameObject.FindGameObjectWithTag("MainCamera").GetComponent<BoardView>().showLog = value;
	}
}
