﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour {
	
	public const int BOARDSIZE = 8;
	public static readonly StartPoint[] STARTPOINTS = {	new StartPoint(3, 3, Player.WHITE),
														new StartPoint(4, 3, Player.BLACK),
														new StartPoint(3, 4, Player.BLACK),
														new StartPoint(4, 4, Player.WHITE)};
	
	public BoardView boardView;
	public bool disableGears;
	public bool disableHints;

	private GameState currentGameState;
	public GameState CurrentGameState {
		get {
			return this.currentGameState;
		}
	}

	private IPlayer[] players;

	void Start() {
		GameParameters gp = GameObject.Find("UI").GetComponent<GameParameters>();

		players = new IPlayer[2];
		players[(int) Player.BLACK] = gp.playerOne;
		players[(int) Player.WHITE] = gp.playerTwo;

		currentGameState = new GameState();
		boardView.ClearLog();
		boardView.AppendLog("setting up a new board:");
		toggleTurn();
		boardView.AppendLog("board is set up!");
		players[(int) currentGameState.CurrentPlayer].StartThinking();
	}

	void Update() {
		if (!GetComponent<BoardView>().animationsFinished) return;

		if (currentGameState.CurrentPlayer != Player.NONE) {
			IPlayer player = players[(int) currentGameState.CurrentPlayer];

			if (player.FinishedThinking() ) {
				Point move = player.GetMove();
				currentGameState.ExecuteMove(move);

				if (currentGameState.CurrentPlayer != Player.NONE) {
					toggleTurn();
					players[(int) currentGameState.CurrentPlayer].StartThinking();
				} else {
					boardView.clickHandlersEnabled = false;
					boardView.showValidMoves = false;
					boardView.showGears = false;
					boardView.ShowGameOver();
				}
			}
		}
	}

	private void toggleTurn() {
		if (players[(int) currentGameState.CurrentPlayer] is AIPlayer) {
			boardView.clickHandlersEnabled = false;
			boardView.showValidMoves = false;
			boardView.showGears = true && !disableGears;
		} else {
			boardView.showGears = false;
			boardView.showValidMoves = true && !disableHints;
			boardView.clickHandlersEnabled = true;
		}
	}

	public void OnClick(Point plannedMove) {
		if (!currentGameState.ValidMoves.Contains(plannedMove)) {
			boardView.ShowInvalidMove(plannedMove);
			return;
		}

		HumanPlayer player = players[(int) currentGameState.CurrentPlayer] as HumanPlayer;
		player.SetMove(plannedMove);
	}

	public static Player GetOpponent(Player player) {
		if (player == Player.WHITE) {
			return Player.BLACK;
		}

		if (player == Player.BLACK) {
			return Player.WHITE;
		}

		return Player.NONE;
	}
}
