﻿using UnityEngine;
using System.Collections;

public class DiscAnimationInfo : MonoBehaviour {

	public bool animationFinished = false;

	public void startAnimation() {
		animationFinished = false;
	}

	public void stopAnimation() {
		animationFinished = true;
	}
}
