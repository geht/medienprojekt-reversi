﻿using UnityEngine;
using System.Collections;

public abstract class IPlayer : MonoBehaviour {
	public abstract void StartThinking();
	public abstract bool FinishedThinking();
	public abstract Point GetMove();
}
